import defaults from './config/gdpr';
import { getCookie, setCookie } from './helpers/cookie';
import GdprBar from './components/GdprBar';
import GdprModal from './components/GdprModal';

const VueGdprPlugin = {
    name: 'vue-gdpr-plugin',
    production: process.env.NODE_ENV === 'production',
    install(Vue, options = {}) {
        options = {
            ...defaults,
            ...options,
        };

        // load settings from script[type=application/json]
        const jsonEl = document.getElementById('gdpr-levels');

        if (jsonEl) {
            const levels = JSON.parse(jsonEl.textContent);
            options.levels = {
                ...options.levels,
                ...levels,
            };
        }

        Vue.mixin({
            beforeCreate() {
                this.$gdpr = {
                    /**
                     * Set cookie level and append related scripts.
                     * @param level
                     * @param levels
                     */
                    setLevel(level) {
                        if (!level) {
                            return;
                        }

                        setCookie(options.cookie_name, level);

                        if (options.levels[level].scripts) {
                            this.loadScripts(options.levels[level].scripts);
                        }
                    },

                    /**
                     * Get level from cookie.
                     * @return {any}
                     */
                    getLevel() {
                        return getCookie(options.cookie_name);
                    },

                    /**
                     * Load given scripts in head, body or footer.
                     * @param scripts
                     */
                    loadScripts(scripts) {
                        if (!Array.isArray(scripts)) {
                            scripts = [scripts];
                        }

                        scripts.forEach((script) => {
                            if (typeof script === 'string') {
                                script = {
                                    position: 'head',
                                    content: script,
                                };
                            }

                            const tag =
                                script.position === 'footer'
                                    ? 'body'
                                    : script.position;
                            const method =
                                script.position === 'footer'
                                    ? 'append'
                                    : 'prepend';

                            const el = document.getElementsByTagName(tag)[0];
                            const content = document
                                .createRange()
                                .createContextualFragment(script.content);

                            el[method](content);
                        });
                    },
                    ...options,
                };
            },
        });

        Vue.component('gdpr-bar', GdprBar);
        Vue.component('gdpr-modal', GdprModal);
    },
};

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(VueGdprPlugin);
}

export default VueGdprPlugin;
