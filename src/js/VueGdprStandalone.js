import Vue from 'vue';
import VueGdprPlugin from './VueGdprPlugin';

Vue.use(VueGdprPlugin);

const wrapper = document.createElement('div');
wrapper.className = 'gdpr-wrapper';
document.body.appendChild(wrapper);

new Vue().$mount('.gdpr-wrapper');
