/**
 * Delete cookie.
 * @param name
 */
export function deleteCookie(name) {
    setCookie(name, '', -1);
}

/**
 * Get cookie.
 * @param name
 * @return {any}
 */
export function getCookie(name) {
    const v = document.cookie.match(`(^|;) ?${name}=([^;]*)(;|$)`);
    return v ? decodeURIComponent(v[2]) : null;
}

/**
 * Set cookie.
 * @param name
 * @param value
 * @param days
 */
export function setCookie(name, value, days = 1) {
    const d = new Date();
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
    document.cookie = `${name}=${encodeURIComponent(
        value,
    )};Path=/;Expires=${d.toUTCString()};SameSite=strict`; // ;secure ?
}
