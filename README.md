# Vue GDPR

> Cookie bar with modal preferences and helpers.



## Installation

Make sure all dependencies have been installed before moving on:

- [Node.js](http://nodejs.org/) >= 8.0.0
- [VueJS](https://vuejs.org/) >= 2.x



### NPM

Use our private GDPR component :

```bash
npm install @cherrypulp/vue-gdpr --registry http://npm.cherrypulp.com
```

Or use as a local module :

```bash
git clone git@gitlab.com:cherrypulp/components/vue-gdpr.git ./assets/local_modules/@cherrypulp/vue-gdpr
```

then install :

```bash
npm install file:./assets/local_modules/@cherrypulp/vue-gdpr
```

Register VueGdprPlugin in your Vue application :

```javascript
// main.js - without trunk
import Vue from 'vue';
import VueGdprPlugin from "@cherrypulp/vue-gdpr";

// Add VueGdprPlugin
Vue.use(VueGdprPlugin);

new Vue().$mount('.app');
```

>  To use with [Trunk](https://gitlab.com/cherrypulp/libraries/trunk-framework) refer to [Trunk GDPR](https://gitlab.com/cherrypulp/components/trunk-gdpr).

HTML

```html
<script type="application/json" id="gdpr-levels">{"required":{"title":"Strictly necessary cookies","content":"These cookies are essential for the operation of the site.","scripts":[{"position":"head","content":""},{"position":"body","content":""},{"position":"footer","content":""}]},"third_party":{"title":"3rd party cookies","content":"Keeping this cookie enabled helps us to improve our website.","scripts":[{"position":"head","content":""},{"position":"body","content":""},{"position":"footer","content":""}]}}</script>
<gdpr-modal button-agreement="Save changes" button-close="" title="Cookie settings" content="You can find out more about which cookies we use, or switch them off. Here, you'll also find links to our [gdpr_policy]Cookie Policies[/gdpr_policy], which explain how we process your personal data." label-active="Enabled" label-inactive="Disabled" ></gdpr-modal>
<gdpr-bar button-agreement="Accept" button-preferences="Settings" content="This website uses cookies to provide you the best browsing experience. [gdpr_policy]Find out more[/gdpr_policy] or adjust your [gdpr_settings]settings[/gdpr_settings]." ></gdpr-bar>
```





## Documentation

`VueGdprPlugin` will expose a `$gdpr` object that you can use in you Vue application.

```javascript
...
mounted() {
    const level = this.$gdpr.getLevel();
    
    if (level === 'third_party') {
        this.$gdpr.loadScripts(this.$gdpr.levels[level]);
    }
},
```



### setLevel(level)

Set cookie level and append related scripts.

```javascript
this.$gdpr.setLevel('required');
```



### getLevel()

Get level from cookie.

```javascript
this.$gdpr.getLevel(); // return "required"
```



### loadScripts(scripts)

Load given scripts in head, body or footer.

```javascript
this.$gdpr.loadScripts([
    {
        position: 'head',
        content: '...',
    },
    {
        position: 'body',
        content: '...',
    },
    {
        position: 'footer',
        content: '...',
    },
]);
```



## Versioning

Versioned using [SemVer](http://semver.org/).

## Contribution

Please raise an issue if you find any. Pull requests are welcome!

## Author

  + **Stéphan Zych** - [monkey_monk](https://gitlab.com/monkey_monk)

## License

This project is licensed under the MIT License - see the [LICENSE](https://gitlab.com/cherrypulp/boilerplates/trunk-module/blob/master/LICENSE) file for details.


## TODO

- [ ] unit tests
