module.exports = {
    arrowParens: 'always',
    bracketSpacing: true,
    endOfLine: 'crlf',
    singleQuote: true,
    tabWidth: 4,
    trailingComma: 'all',
    overrides: [
        // {
        //     files: '*.php',
        //     options: {
        //         braceStyle: 'psr-2',
        //         parser: 'php',
        //         trailingCommaPHP: 'all',
        //     },
        // },
        {
            files: '*.js',
            options: {
                parser: 'babel',
            },
        },
        {
            files: '*.json',
            options: {
                parser: 'json',
                singleQuote: false,
                tabWidth: 2,
                trailingComma: 'none',
            },
        },
        {
            files: '*.scss',
            options: {
                parser: 'scss',
            },
        },
        {
            files: '*.vue',
            options: {
                parser: 'vue',
                vueIndentScriptAndStyle: false,
            },
        },
    ],
};
