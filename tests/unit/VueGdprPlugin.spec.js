import Vue from 'vue/dist/vue';
import { expect } from 'chai';
import { createLocalVue, mount } from '@vue/test-utils';
import VueGdprPlugin from '../../dist/js/VueGdprPlugin';

Vue.use(VueGdprPlugin);

describe('VueGdprPlugin', () => {
    let localVue;

    beforeEach(() => {
        localVue = createLocalVue();
        localVue.use(VueGdprPlugin);
    });

    it('use VueGdprPlugin', () => {
        const template = `<div>
            <script type="application/json" id="gdpr-levels">
                {"required":{"title":"Strictly necessary cookies","content":"These cookies are essential for the operation of the site.","scripts":[{"position":"head","content":""},{"position":"body","content":""},{"position":"footer","content":""}]},"third_party":{"title":"3rd party cookies","content":"Keeping this cookie enabled helps us to improve our website.","scripts":[{"position":"head","content":""},{"position":"body","content":""},{"position":"footer","content":""}]}}
            </script>
            <gdpr-modal button-agreement="Save changes" button-close="" title="Cookie settings" content="You can find out more about which cookies we use, or switch them off. Here, you'll also find links to our [policy]Cookie Policies[/policy], which explain how we process your personal data." label-active="Enabled" label-inactive="Disabled" ></gdpr-modal>
            <gdpr-bar button-agreement="Accept" button-preferences="Settings" content="This website uses cookies to provide you the best browsing experience. [gdpr_policy]Find out more[/gdpr_policy] or adjust your [gdpr_settings]settings[/gdpr_settings]." ></gdpr-bar>
        </div>`;

        const wrapper = mount({ template }, { localVue });
        console.log('--', wrapper.html());
    });
});
