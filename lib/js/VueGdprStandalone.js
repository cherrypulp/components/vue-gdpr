'use strict';

var _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault');

var _vue = _interopRequireDefault(require('vue'));

var _VueGdprPlugin = _interopRequireDefault(require('./VueGdprPlugin'));

_vue['default'].use(_VueGdprPlugin['default']);

var wrapper = document.createElement('div');
wrapper.className = 'gdpr-wrapper';
document.body.appendChild(wrapper);
new _vue['default']().$mount('.gdpr-wrapper');
