'use strict';

var _interopRequireDefault = require('@babel/runtime/helpers/interopRequireDefault');

Object.defineProperty(exports, '__esModule', {
    value: true,
});
exports['default'] = void 0;

var _defineProperty2 = _interopRequireDefault(
    require('@babel/runtime/helpers/defineProperty'),
);

var _gdpr = _interopRequireDefault(require('./config/gdpr'));

var _cookie = require('./helpers/cookie');

var _GdprBar = _interopRequireDefault(require('./components/GdprBar'));

var _GdprModal = _interopRequireDefault(require('./components/GdprModal'));

function ownKeys(object, enumerableOnly) {
    var keys = Object.keys(object);
    if (Object.getOwnPropertySymbols) {
        var symbols = Object.getOwnPropertySymbols(object);
        if (enumerableOnly)
            symbols = symbols.filter(function(sym) {
                return Object.getOwnPropertyDescriptor(object, sym).enumerable;
            });
        keys.push.apply(keys, symbols);
    }
    return keys;
}

function _objectSpread(target) {
    for (var i = 1; i < arguments.length; i++) {
        var source = arguments[i] != null ? arguments[i] : {};
        if (i % 2) {
            ownKeys(Object(source), true).forEach(function(key) {
                (0, _defineProperty2['default'])(target, key, source[key]);
            });
        } else if (Object.getOwnPropertyDescriptors) {
            Object.defineProperties(
                target,
                Object.getOwnPropertyDescriptors(source),
            );
        } else {
            ownKeys(Object(source)).forEach(function(key) {
                Object.defineProperty(
                    target,
                    key,
                    Object.getOwnPropertyDescriptor(source, key),
                );
            });
        }
    }
    return target;
}

var VueGdprPlugin = {
    name: 'vue-gdpr-plugin',
    production: process.env.NODE_ENV === 'production',
    install: function install(Vue) {
        var options =
            arguments.length > 1 && arguments[1] !== undefined
                ? arguments[1]
                : {};
        options = _objectSpread(_objectSpread({}, _gdpr['default']), options); // load settings from script[type=application/json]

        var jsonEl = document.getElementById('gdpr-levels');

        if (jsonEl) {
            var levels = JSON.parse(jsonEl.textContent);
            options.levels = _objectSpread(
                _objectSpread({}, options.levels),
                levels,
            );
        }

        Vue.mixin({
            beforeCreate: function beforeCreate() {
                this.$gdpr = _objectSpread(
                    {
                        /**
                         * Set cookie level and append related scripts.
                         * @param level
                         * @param levels
                         */
                        setLevel: function setLevel(level) {
                            if (!level) {
                                return;
                            }

                            (0, _cookie.setCookie)(options.cookie_name, level);

                            if (options.levels[level].scripts) {
                                this.loadScripts(options.levels[level].scripts);
                            }
                        },

                        /**
                         * Get level from cookie.
                         * @return {any}
                         */
                        getLevel: function getLevel() {
                            return (0, _cookie.getCookie)(options.cookie_name);
                        },

                        /**
                         * Load given scripts in head, body or footer.
                         * @param scripts
                         */
                        loadScripts: function loadScripts(scripts) {
                            if (!Array.isArray(scripts)) {
                                scripts = [scripts];
                            }

                            scripts.forEach(function(script) {
                                if (typeof script === 'string') {
                                    script = {
                                        position: 'head',
                                        content: script,
                                    };
                                }

                                var tag =
                                    script.position === 'footer'
                                        ? 'body'
                                        : script.position;
                                var method =
                                    script.position === 'footer'
                                        ? 'append'
                                        : 'prepend';
                                var el = document.getElementsByTagName(tag)[0];
                                var content = document
                                    .createRange()
                                    .createContextualFragment(script.content);
                                el[method](content);
                            });
                        },
                    },
                    options,
                );
            },
        });
        Vue.component('gdpr-bar', _GdprBar['default']);
        Vue.component('gdpr-modal', _GdprModal['default']);
    },
};

if (typeof window !== 'undefined' && window.Vue) {
    window.Vue.use(VueGdprPlugin);
}

var _default = VueGdprPlugin;
exports['default'] = _default;
