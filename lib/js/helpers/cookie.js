'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true,
});
exports.deleteCookie = deleteCookie;
exports.getCookie = getCookie;
exports.setCookie = setCookie;

/**
 * Delete cookie.
 * @param name
 */
function deleteCookie(name) {
    setCookie(name, '', -1);
}
/**
 * Get cookie.
 * @param name
 * @return {any}
 */

function getCookie(name) {
    var v = document.cookie.match('(^|;) ?'.concat(name, '=([^;]*)(;|$)'));
    return v ? decodeURIComponent(v[2]) : null;
}
/**
 * Set cookie.
 * @param name
 * @param value
 * @param days
 */

function setCookie(name, value) {
    var days =
        arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : 1;
    var d = new Date();
    d.setTime(d.getTime() + 24 * 60 * 60 * 1000 * days);
    document.cookie = ''
        .concat(name, '=')
        .concat(encodeURIComponent(value), ';Path=/;Expires=')
        .concat(d.toUTCString(), ';SameSite=strict'); // ;secure ?
}
