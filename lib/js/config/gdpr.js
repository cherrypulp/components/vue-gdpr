'use strict';

Object.defineProperty(exports, '__esModule', {
    value: true,
});
exports['default'] = void 0;
var _default = {
    /**
     * Cookie name.
     */
    cookie_name: 'gdpr_level',

    /**
     * Default bar visibility.
     */
    isBarVisible: true,

    /**
     * Default modal visibility.
     */
    isModalVisible: false,

    /**
     * Levels definition.
     */
    levels: {
        /*required: {
        title: '',
        content: '',
        scripts: [
            {
                position: 'head',
                content: '',
            },
            {
                position: 'body',
                content: '',
            },
            {
                position: 'footer',
                content: '',
            },
        ],
    },*/
    },
};
exports['default'] = _default;
